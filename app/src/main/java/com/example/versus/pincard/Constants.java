package com.example.versus.pincard;

/**
 * Created by versus on 1/17/16.
 */
public class Constants {
    public static final int CARD_ROWS = 6;
    public static final int CARD_COLUMNS = 6;

    public static final String DATABASE_NAME ="pincards.db";
    public static final int DATABASE_VERSION = 1;
    public static final String PINCARD_DB_TABLE_NAME = "cards";
    public static final String TABLE_CARDS_ID_COLUMN_NAME = "_id";
    public static final String TABLE_CARDS_DATA_COLUMN_NAME = "card_data";

    public final static String INTENT_PINCARD_ID = "com.example.versus.pincard.INTENT_ID";
    public final static int TABLE_CYPHER_SIZE=30;

}
