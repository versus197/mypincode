package com.example.versus.pincard.ui.presenter;

import android.content.Context;
import android.util.Log;

import com.example.versus.pincard.model.PinCard;
import com.example.versus.pincard.model.PinCardDataSource;
import com.example.versus.pincard.ui.view.CreatePinCardView;

/**
 * Created by versus on 12/13/15.
 */

public class CreatePinCardPresenterImpl implements CreatePinCardPresenter {

    CreatePinCardView rootView;
    PinCardDataSource dbcard;

    public CreatePinCardPresenterImpl(CreatePinCardView rootView) {
        this.rootView = rootView;
    }

    @Override
    public PinCard createPinCard() {
    return  null;
    }

    @Override
    public boolean savePinCard(PinCard card) {
        Log.d("CreatePinCardActivity", "Save pinCard = " + card.toJson());
        dbcard = new PinCardDataSource((Context) rootView);
        dbcard.open();
        dbcard.insertPinCard(card);
        dbcard.close();
        return false;
    }

    @Override
    public boolean dropPinCard(PinCard card) {
        return false;
    }

    @Override
    public boolean addTitlePinCard(PinCard card) {
        return false;
    }

    @Override
    public boolean changeTitlePinCard(PinCard card) {
        return false;
    }

    @Override
    public void onStart(long pinCardId) {
        Log.d("Create PinCard Presenter", "pincard id = " + pinCardId);
    }


    private int[] stringToIntArray(String line) {
        int index = 0;
        int[] resultArray = new int[line.length()];

        for (int i = 0, n = line.length(); i < n; i++) {
            char c = line.charAt(i);
            if (Character.isDigit(c)) {
                resultArray[index] = Character.getNumericValue(c);
                index++;
            }
        }
        return resultArray;
    }

    @Override
    public int getPinCodeCipher(String pin, int nextIntIndex) {
        int[] arr = stringToIntArray(pin);
        int ret = -1;
        if(nextIntIndex < arr.length){
            ret = arr[nextIntIndex];
        }
        return ret;
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }
}
