package com.example.versus.pincard.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.versus.pincard.Constants;

/**
 * Created by versus on 7/27/16.
 */
public class PinCardSQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_CREATE = "create table "
            + Constants.PINCARD_DB_TABLE_NAME + "( " + Constants.TABLE_CARDS_ID_COLUMN_NAME
            + " integer primary key autoincrement, " + Constants.TABLE_CARDS_DATA_COLUMN_NAME
            + " text not null);";


    public PinCardSQLiteHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("PinCardSQLiteHelper", "onCreate= " + DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.PINCARD_DB_TABLE_NAME);
        db.execSQL(DATABASE_CREATE);
    }
}
