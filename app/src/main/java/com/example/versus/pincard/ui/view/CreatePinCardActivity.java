package com.example.versus.pincard.ui.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.versus.myapplication.R;
import com.example.versus.pincard.Constants;
import com.example.versus.pincard.PinCardApplication;
import com.example.versus.pincard.model.PinCard;
import com.example.versus.pincard.model.PinCardDataSource;
import com.example.versus.pincard.ui.presenter.CreatePinCardPresenterImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;


public class CreatePinCardActivity extends AppCompatActivity implements CreatePinCardView {

    CreatePinCardPresenterImpl presenter;
    PinCard pinCard;
    int [][] pCard;
    String [][] pCardColor;
    int nextIntIndex=0;


    @BindView(R.id.editPin) EditText pin;
    @BindView(R.id.editTitle) EditText title;
    @BindView(R.id.pinCardTitle) TextView pinCardTitle;
    @OnTextChanged(R.id.editTitle)
    void onTitleTextChanged(CharSequence ttitile, int start, int count, int after) {
        pinCardTitle.setText(ttitile);
        pinCard.setPinCardTitle(ttitile.toString());
    }
    @BindView(R.id.CreatePinCardTable) TableLayout pincard_table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createpincard);
        ButterKnife.bind(this);
        presenter = new CreatePinCardPresenterImpl(this);

        Intent intent = getIntent();
        long pinCardId = intent.getLongExtra(Constants.INTENT_PINCARD_ID, -1);
        presenter.onStart(pinCardId);

        pinCard = new PinCard("Bank #", Constants.CARD_ROWS, Constants.CARD_COLUMNS);
        pCard = pinCard.genNulPinCard();
        pCardColor =  pinCard.getPinCardColor();
        pinCard.setPinCardColor(pinCard.genPinCardColor());
        showTable();


    }

    @Override
     public void showTable(){
        TableLayout tableLayout = new TableLayout(this);
        tableLayout.setStretchAllColumns(true);
        tableLayout.setPadding(20, 5, 20, 10);
        TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams();
        tableRowParams.setMargins(1, 1, 1, 1);
        tableRowParams.weight = 1;
        for (int i = 0; i < Constants.CARD_ROWS; i++) {
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            for (int j = 0; j < Constants.CARD_COLUMNS; j++) {
                TextView tv = new TextView(this);
                tv.setTextSize(Constants.TABLE_CYPHER_SIZE);
                if(pCard[i][j] == -1){
                    tv.setText(String.valueOf(" "));
                } else {
                tv.setText(String.valueOf(pCard[i][j]));
                }
                tv.setBackgroundColor(Color.parseColor(pCardColor[i][j]));
                tv.setGravity(Gravity.CENTER);
                final int finalI = i;
                final int finalJ = j;
                tv.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        TextView cell = (TextView) v;
                        int nextInt = presenter.getPinCodeCipher(pin.getText().toString(), nextIntIndex);
                        if (nextInt > -1) {
                            cell.setText(String.valueOf(nextInt));
                            nextIntIndex++;
                            pCard[finalI][finalJ] = nextInt;
                            Log.d("CreatePinCardActivity", "Table position is " + finalI + " " + finalJ + " pinCard = " + pinCard.toJson());
                        }
                    }
                });

                tableRow.addView(tv, tableRowParams);
            }
            tableLayout.addView(tableRow, i);
        }
        pincard_table.removeAllViews();
        pincard_table.addView(tableLayout);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_generate_new_card: {
                generateCard();
                return true;
            }
            case R.id.action_save_new_card: {
                generateCard();
                presenter.savePinCard(pinCard);
                finish();
                return true;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void generateCard(){
        pCard = pinCard.changeNulPinCard();
        showTable();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }


}
