package com.example.versus.pincard.ui.presenter;

import com.example.versus.pincard.model.PinCard;

/**
 * Created by versus on 12/13/15.
 */
public interface CreatePinCardPresenter extends  Presenter{

    PinCard  createPinCard();

    boolean savePinCard(PinCard card);
    int getPinCodeCipher(String pin, int nextIntIndex);


    boolean dropPinCard(PinCard card);

    boolean addTitlePinCard(PinCard card);

    boolean  changeTitlePinCard(PinCard card);

    void onStart(long pinCardId);
}
