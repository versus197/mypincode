package com.example.versus.pincard.ui.presenter;

/**
 * Created by versus on 12/1/15.
 */
public interface MainPresenter extends Presenter{
    void onStart();

    void createPinCard();
    void pause();
    void resume();
    void destroy();
}
