package com.example.versus.pincard.ui.presenter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.versus.pincard.Constants;
import com.example.versus.pincard.PinCardApplication;
import com.example.versus.pincard.model.PinCard;
import com.example.versus.pincard.model.PinCardDataSource;
import com.example.versus.pincard.ui.view.CreatePinCardActivity;
import com.example.versus.pincard.ui.view.MainView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by versus on 12/1/15.
 */
public class MainPresenterImpl implements MainPresenter {

    MainView rootView;
    PinCardDataSource dbcard;
    List<PinCard> records = new ArrayList<PinCard>();

    public MainPresenterImpl(MainView rootView) {
        this.rootView = rootView;
    }

    @Override
    public void resume() {
        dbcard.open();
        rootView.updateAdapter(dbcard.getAllCards());
    }

    @Override
    public void pause() {
        dbcard.close();
    }

    @Override
    public void destroy() {
        dbcard.close();

    }

    @Override
    public void onStart() {
        dbcard = new PinCardDataSource((Context) rootView);
        dbcard.open();
        records = dbcard.getAllCards();
        Log.d("Main Presenter", "records size = " + records.size());
        rootView.createAdapter(records);
        if(records.size() == 0) {
            createPinCard();
        }
    }

    @Override
    public void createPinCard() {
        Intent intent = new Intent((Context) rootView, CreatePinCardActivity.class);
       ((Context) rootView).startActivity(intent);
        rootView.updateAdapter(dbcard.getAllCards());
    }


    public List<PinCard> getRecords() {
        return records;
    }
}
