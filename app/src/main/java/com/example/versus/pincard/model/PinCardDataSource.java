package com.example.versus.pincard.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.versus.pincard.Constants;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by versus on 7/27/16.
 */
public class PinCardDataSource {
    private SQLiteDatabase database;
    private PinCardSQLiteHelper dbHelper;
    private String[] allColumns = {Constants.TABLE_CARDS_ID_COLUMN_NAME, Constants.TABLE_CARDS_DATA_COLUMN_NAME};

    public PinCardDataSource(Context context) {
        dbHelper = new PinCardSQLiteHelper(context);

    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        Log.d("PinCardDataSource", "dbHelper open");
    }

    public void close() {
        dbHelper.close();
        Log.d("PinCardDataSource", "dbHelper close");
    }

    public long insertPinCard(PinCard pincard) {
        Log.d("PinCardDataSource", "insert");
        ContentValues values = new ContentValues();
        values.put(Constants.TABLE_CARDS_DATA_COLUMN_NAME, pincard.toJson());
        long insertId = database.insert(Constants.PINCARD_DB_TABLE_NAME, null, values);
        return insertId;
    }

    public void updatePinCard(PinCard pinCard){
        ContentValues newValues = new ContentValues();
        newValues.put(Constants.TABLE_CARDS_DATA_COLUMN_NAME, pinCard.toJson());
        database.update(Constants.PINCARD_DB_TABLE_NAME, newValues, "_id="+pinCard.getId(), null);
        Log.d("PinCardDataSource", "Update pinCard id="+pinCard.getId() + " pincard=" + pinCard.toJson());
    }

    public void deletePinCard(PinCard pincard) {
        long id = pincard.getId();
        database.delete(Constants.PINCARD_DB_TABLE_NAME, Constants.TABLE_CARDS_ID_COLUMN_NAME + " = " + id, null);
        Log.d("PinCardDataSource", "delete id="+id);
    }

    public List<PinCard> getAllCards(){
        List<PinCard> records = new ArrayList<PinCard>();
        Cursor cursor = database.query(Constants.PINCARD_DB_TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PinCard pinCard = null;
            try {
                pinCard = new PinCard(cursor.getLong(0), cursor.getString(1));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            records.add(pinCard);
            cursor.moveToNext();
        }
        return records;
    }


}
