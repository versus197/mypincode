package com.example.versus.pincard;

import android.app.Application;
import android.content.Context;

/**
 * Created by versus on 7/27/16.
 */
public class PinCardApplication extends Application {

    private static PinCardApplication instance = null;

    public PinCardApplication() {
        instance = this;
    }

    public static Context getContext()
    {
        if (null == instance)
        {
            instance = new PinCardApplication();
        }

        return instance;
    }
}
