package com.example.versus.pincard.ui.view;

import com.example.versus.pincard.model.PinCard;

import java.util.List;

/**
 * Created by versus on 12/1/15.
 */
public interface MainView {
    void createAdapter(List<PinCard> records);
    void updateAdapter(List<PinCard> records);
}
