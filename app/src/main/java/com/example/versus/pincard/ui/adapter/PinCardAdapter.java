package com.example.versus.pincard.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
//import android.support.v7.app.AlertDialog;
import androidx.appcompat.app.AlertDialog;
//import android.support.v7.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.versus.myapplication.R;
import com.example.versus.pincard.Constants;
import com.example.versus.pincard.PinCardApplication;
import com.example.versus.pincard.model.PinCard;
import com.example.versus.pincard.model.PinCardDataSource;
import com.example.versus.pincard.ui.view.CreatePinCardActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PinCardAdapter extends RecyclerView.Adapter<PinCardAdapter.PinCardViewHolder>{


    private List<PinCard> cards;
    private Context pContext;
    public PinCardAdapter(List<PinCard> cards) {
        this.cards = cards;
    }
    private PinCardDataSource dbcard ;

    @Override
    public PinCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        pContext = parent.getContext();
        dbcard = new PinCardDataSource(PinCardApplication.getContext());
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pincarditem, parent, false);
        PinCardAdapter.PinCardViewHolder vh = new PinCardViewHolder(v, new PinCardAdapter.PinCardViewHolder.IPinCardViewHolderClicks() {
            @Override
            public void onShow(View caller, int position) {
                Intent intent = new Intent(pContext, CreatePinCardActivity.class);
                intent.putExtra(Constants.INTENT_PINCARD_ID, cards.get(position).getId());
                pContext.startActivity(intent);
            }

            @Override
            public void onEdit(View caller, int position) {
                Log.d("Adapter", "Edit card position = " + position + " pincard id = "+ cards.get(position).getId());
                Intent intent = new Intent(pContext, CreatePinCardActivity.class);
                intent.putExtra(Constants.INTENT_PINCARD_ID, cards.get(position).getId());
                pContext.startActivity(intent);
            }

            @Override
            public void onDelete(View caller, final int position) {
                Log.d("Adapter", "Delete card position = " + position);
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                dbcard.open();
                                dbcard.deletePinCard(cards.get(position));
                                cards.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position,cards.size());
                                dbcard.close();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //Do your No progress
                                break;
                        }
                    }
                };
                AlertDialog.Builder ab = new AlertDialog.Builder(pContext);
                ab.setMessage("Are you sure to delete?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(PinCardViewHolder holder, int position) {
        PinCard oneCard = cards.get(position);
        Log.d("pinCardAdapter", "PinCard = " + oneCard.toJson());
        String [][] pCardColor = oneCard.getPinCardColor();
        int [][] pCard = oneCard.getPinCardCode();
        TableLayout tableLayout = new TableLayout(pContext);
        tableLayout.setStretchAllColumns(true);
        tableLayout.setPadding(20, 5, 20, 10);
        TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams();
        tableRowParams.setMargins(1, 1, 1, 1);
        tableRowParams.weight = 1;
        for (int i = 0; i < Constants.CARD_ROWS; i++) {
            TableRow tableRow = new TableRow(pContext);
            tableRow.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            for (int j = 0; j < Constants.CARD_COLUMNS; j++) {
                TextView tv = new TextView(pContext);
                tv.setTextSize(Constants.TABLE_CYPHER_SIZE);
                tv.setText(String.valueOf(pCard[i][j]));
                tv.setBackgroundColor(Color.parseColor(pCardColor[i][j]));
                tv.setGravity(Gravity.CENTER);
                tableRow.addView(tv, tableRowParams);
            }
            tableLayout.addView(tableRow, i);
        }
        holder.hTitle.setText(oneCard.getPinCardTitle());
        holder.hTable.removeAllViews();
        holder.hTable.addView(tableLayout);
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public static class PinCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.itemTitle) TextView hTitle;
        @BindView(R.id.itemCardTable) TableLayout hTable;
        @BindView(R.id.action_delete_pincard) ImageView hDelete;
        //@BindView(R.id.action_edit_pincard) ImageView hEdit;

        public IPinCardViewHolderClicks mListener;

        public PinCardViewHolder(View itemView, IPinCardViewHolderClicks listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
            //hTable.setOnClickListener(this);
            hTitle.setOnClickListener(this);
            //hEdit.setOnClickListener(this);
            hDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(final View view) {
            switch (view.getId()) {
                case R.id.action_delete_pincard:
                    mListener.onDelete(view, getLayoutPosition());
                    break;
                /*
                case R.id.action_edit_pincard:
                    mListener.onEdit(view, getLayoutPosition());
                    break;
                    */
                default:
                    mListener.onShow(view, getLayoutPosition());
                    break;
            }
        }

        public interface IPinCardViewHolderClicks {
            void onShow(View caller, int layoutPosition);
            void onEdit(View caller, int layoutPosition);
            void onDelete(View caller, int layoutPosition);
        }
    }

}
