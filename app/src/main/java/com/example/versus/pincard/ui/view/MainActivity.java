package com.example.versus.pincard.ui.view;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.versus.myapplication.R;
import com.example.versus.pincard.model.PinCard;
import com.example.versus.pincard.ui.adapter.PinCardAdapter;
import com.example.versus.pincard.ui.presenter.MainPresenterImpl;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView {
    MainPresenterImpl presenter;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    PinCardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenterImpl(this);
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_add_new_card:{
                Log.d("Main Activity", "Click on add new card");
                presenter.createPinCard();
                break;
            }
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void createAdapter(List<PinCard> records) {
        adapter = new PinCardAdapter(records);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(itemAnimator);
    }

    @Override
    public  void  updateAdapter(List<PinCard> records){
            recyclerView.setAdapter(new PinCardAdapter(records));
            recyclerView.invalidate();
    }
}
