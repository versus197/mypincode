package com.example.versus.pincard.model;

import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Random;

public class PinCard {
    final static String[] colors= { "#EF9A9A", "#E1BEE7", "#D1C4E9", "#C5CAE9", "#BBDEFB",
            "#90CAF9", "#448AFF", "#B2DFDB", "#80CBC4", "#E6EE9C",
            "#FFF59D", "#FFAB91", "#D7CCC8", "#B9F6CA", "#B0BEC5", "#FFCC80"
    };
    private long pinCardId;
    private int [][] pinCardCode;
    private String [][] pinCardColor;
    private int rows;
    private int cols;

    public void setPinCardTitle(String pinCardTitle) {
        this.pinCardTitle = pinCardTitle;
    }

    private String pinCardTitle;

    public PinCard(String title, int rows, int cols) {
        this.pinCardTitle = title;
        this.rows = rows;
        this.cols = cols;
        pinCardCode = new int[rows][cols];
        pinCardColor = new String[rows][cols];
    }

    public PinCard(long id, String json) throws JSONException {
        Log.d("PinCard Constructor", "Constructor " + json);
        this.pinCardId = id;
        JSONObject jObj = new JSONObject(json);
        this.pinCardTitle = jObj.getString("title");
        this.rows = Integer.parseInt(jObj.getString("rows"));
        this.cols = Integer.parseInt(jObj.getString("cols"));
        this.pinCardCode = new int[rows][cols];
        this.pinCardColor = new String[rows][cols];
        JSONArray jArr = jObj.getJSONArray("pinCardCode");
        int k=0;
        for (int i = 0; i < rows; i++) {
            for(int j =0; j< cols; j++) {
                JSONObject obj = jArr.getJSONObject(k);
                this.pinCardCode[i][j] =Integer.parseInt(obj.getString("num"));
                this.pinCardColor[i][j]= obj.getString("color");
                //Log.d("PinCard Constructor", "num " + obj.getString("num") + " color "+ obj.getString("color"));
                k++;
            }
        }
    }

    public String[][] genPinCardColor(){
        Random rand = new Random();
        for (int i = 0; i < rows; i++) {
            for(int j =0; j< cols; j++){
                pinCardColor[i][j]=colors[rand.nextInt(Array.getLength(colors))];
            }
        }
        return pinCardColor;
    }

    public int[][] genNulPinCard(){
        Random rand = new Random();
        for (int i = 0; i < rows; i++) {
            for(int j =0; j< cols; j++){
                pinCardCode[i][j]=-1;
            }
        }
        return pinCardCode;
    }

    public int[][] changeNulPinCard(){
        Random rand = new Random();
        for (int i = 0; i < rows; i++) {
            for(int j =0; j< cols; j++){
                if(pinCardCode[i][j] == -1) {
                    pinCardCode[i][j] = rand.nextInt(10);
                }
            }
        }
        return pinCardCode;
    }

    public int[][] getPinCardCode() {
        return pinCardCode;
    }

    public String[][] getPinCardColor() {
        return pinCardColor;
    }

    public void setPinCardColor(String[][] pinCardColor){
        this.pinCardColor = pinCardColor;
    }

    public void setPinCardCode(int[][] pinCardCode) {
        this.pinCardCode = pinCardCode;
    }

    public String toJson(){
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("title", this.pinCardTitle);
            jsonObj.put("rows", this.rows);
            jsonObj.put("cols", this.cols);
            JSONArray jsonArrCardCode = new JSONArray();
            for (int i = 0; i < rows; i++) {
                for(int j =0; j< cols; j++){
                    JSONObject pnObj = new JSONObject();
                    pnObj.put("num",pinCardCode[i][j]);
                    pnObj.put("color",pinCardColor[i][j]);
                    jsonArrCardCode.put(pnObj);
                }
            }
            jsonObj.put("pinCardCode", jsonArrCardCode);
            return jsonObj.toString();
        }
        catch(JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public String getPinCardTitle() {
        return pinCardTitle;
    }

    public long getId() {
        return pinCardId;
    }
}
